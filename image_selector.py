#!/usr/bin/env python3

from PyQt5 import QtCore, QtGui, QtWidgets
from image_selector_ui import Ui_MainWindow
import sys, os
from pathlib import Path
import shutil

from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import numpy as np

datagen = ImageDataGenerator(
    rotation_range =40,
    width_shift_range=0.2,
    height_shift_range=0.2,
    rescale=1./255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest')

app = QtWidgets.QApplication( sys.argv )
app.setApplicationName( 'Image Selector' )
scriptDir = os.path.dirname(os.path.realpath(__file__))
app.setWindowIcon(QtGui.QIcon(scriptDir + os.path.sep +'icon.png'))

#global param for save PyQt settings
settings = QtCore.QSettings('cairo', 'image_selector')
folder_path=''
output_path=''
image_data=[]
selected_image=[]
idx=0

# Create a class for our main window
class Main(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        
        # This is always the same
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.retranslateUi

        #setup key event
        QtWidgets.QShortcut(QtCore.Qt.Key_Left, self, self.prev_img)
        QtWidgets.QShortcut(QtCore.Qt.Key_Right, self, self.next_img)
        QtWidgets.QShortcut(QtCore.Qt.Key_Space, self, self.select_img)

        self.ui.open_button.clicked.connect(self.open_folder)
        self.ui.output_button.clicked.connect(self.output_folder)
        self.ui.select_button.clicked.connect(self.select_img)

        self.ui.select_button.setEnabled(False)
        self.ui.similarity_button.setEnabled(False)

        self.restore_settings()

    def prev_img(self):
        global idx
        if idx>0:
            idx-=1
            settings.setValue('index', idx)
            self.show_image()
        else :
            print("no previous image")

    def next_img(self):
        global idx
        if idx<len(image_data)-1:
            idx+=1
            settings.setValue('index', idx)
            self.show_image()
        else :
            print("no next image")

    def select_img(self):
        image_path = image_data[idx]
        if self.ui.select_button.isEnabled() and not image_path in selected_image:
            self.create_augment(image_path)

    def show_image(self):
        image=image_data[idx]
        pixmap = QtGui.QPixmap(image)
        self.ui.image_label.setPixmap(pixmap)
        if image in selected_image:
            status = "already selected "
        else :
            status = " available "
        image_position=str(idx+1)+'/'+str(len(image_data))
        self.add_status(status+image_position)
        img_folder = os.path.dirname(image).split('/').pop()
        self.ui.class_label.setText(img_folder)

    def add_status(self,status):
        self.ui.status_label.setText(status)

    def restore_settings(self):
        global folder_path,output_path, image_data, selected_image, idx
        folder_path = settings.value('image_path')
        if folder_path:
            self.ui.img_path_label.setText(folder_path)
            self.ui.similarity_button.setEnabled(True)

        output_path = settings.value('output_path')
        if output_path:
            self.output_folder(output_path)

        image_data = settings.value('image_data', type='QStringList')
        idx = settings.value('index',type=int)
        if image_data:
            count_img = str(len(image_data))
            self.ui.count_img_label.setText(count_img)
            self.show_image()

        selected_image = settings.value('selected_image', type='QStringList')
        if selected_image:    
            count_selected = str(len(selected_image))
            self.ui.count_select_label.setText(count_selected)

    def open_folder(self):
        home_folder = os.getenv('HOME')
        selected_path = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Image Directory",home_folder))
        if selected_path:
            global folder_path, image_data, selected_image, idx
            folder_path=selected_path
            image_data = self.get_all_file(folder_path)
            count_img = str(len(image_data))
            selected_image=[]
            idx=0
            settings.setValue('image_path', str(folder_path))
            settings.setValue('image_data', image_data)
            settings.setValue('selected_image', selected_image)
            settings.setValue('index', idx)

            self.ui.img_path_label.setText(folder_path)
            self.ui.count_img_label.setText(count_img)
            self.ui.similarity_button.setEnabled(True)
            self.show_image()

    def output_folder(self,selected_path=None):
        home_folder = os.getenv('HOME')
        if not selected_path:
            selected_path = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Output Directory",home_folder))
        if selected_path:
            global output_path
            output_path=selected_path
            settings.setValue('output_path', str(output_path))
            self.ui.output_path_label.setText(output_path)
            self.ui.select_button.setEnabled(True)

    def get_all_file(self,folder):
        files_path = []
        # Walk through all files in the directory that contains the files to copy
        for root, dirs, files in os.walk(folder):
            for filename in files:
                base, extension = os.path.splitext(filename)
                image_extensions = [".jpg",".jpeg",".png"]

                if extension in image_extensions:
                    # I use absolute path, case you want to move several dirs.
                    files_path.append(os.path.join(os.path.abspath(root), filename))

        return files_path
    
    def create_augment(self,image_path):
        img = load_img(image_path)
        img_folder = os.path.dirname(image_path).split('/').pop()
        save_dir=Path(os.path.join(output_path,img_folder))
        save_dir.mkdir(parents=True, exist_ok=True)
        x = img_to_array(img)
        x = x.reshape((1,) + x.shape)
        i = 0

        #Copy original image
        img_filename = os.path.basename(image_path)
        new_name = os.path.join(save_dir.as_posix(),img_filename)
        shutil.copy(image_path, new_name)

        #datagen.flow generate augmented images
        for batch in datagen.flow(x, batch_size=1,save_to_dir=save_dir.as_posix(),save_prefix='augment',save_format='jpg'):

            i += 1
            if i>5:
                break
        global selected_image
        selected_image.append(image_path)
        settings.setValue('selected_image', selected_image)
        image_position=str(idx+1)+'/'+str(len(image_data))
        self.add_status("already selected "+image_position)
        self.ui.count_select_label.setText(str(len(selected_image)))

if __name__ == "__main__":
    shouldrun=True
    window = Main()
    window.show()

    app.lastWindowClosed.connect(app.quit)
    # execute application
    sys.exit( app.exec_() )