## Step to use this app

1. Use your favorite python to run it
`python image_selector.py`

2. Choose Image Folder to select, press **Open Folder**. Usually the folder contain class sub-folder
```
image-folder
    |__class_1
    |__class_x
        |__original_image
```


3. Choose Output Folder, press **Output Folder**

4. Use *Left* and *Right Arrow* to change the image

5. If it's the image that you want, press **Select**. Or use *Space Bar* for handy shortcut

6. In the output folder will generate sub-folder as the original image and will produce 6 Augmented images with the original image
```
output-folder
    |__class_x
        |__original_image
        |__augment_1
        |__ ....
        |__augment_6
```
